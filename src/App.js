import React from 'react';
import './App.css';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Navbar from './components/NavBar/Navbar';
import Dashboard from './components/dashboard/Dashboard';
import FavouriteBooksList from './components/favouriteScreen/listFavouriteBooks';
import Footer  from './components/footer/Footer';

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <div>
          <Navbar />
          <Switch>
            <Route exact path='/' component={Dashboard} />
            <Route path='/favourites' component={FavouriteBooksList} />
          </Switch>
        </div>
        <Footer />
      </div>
    </BrowserRouter >
  );
}

export default App;
