const filterYearReducer = (state = '', action) => {
    switch (action.type) {
        case 'FILTER_YEAR':
            const wordFilter = action.data;
            return wordFilter;
        case 'REMOVE_FILTER_YEAR':
            return '';
        default:
            return state;
    }
}

export default filterYearReducer;