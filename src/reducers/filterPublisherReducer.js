const filterPublisherReducer = (state = '', action) => {
    switch (action.type) {
        case 'FILTER_PUBLISHER':
            const wordFilter = action.data;
            return wordFilter;
        case 'REMOVE_FILTER_PUBLISHER':
            return '';
        default:
            return state;
    }
}

export default filterPublisherReducer;