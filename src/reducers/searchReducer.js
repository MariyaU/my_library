const searchReducer = (state = [], action) => {
    switch (action.type) {
        case 'ADD_BOOKS_COLLECTION':
            if (!action.data) return [];
            const newState = action.data;
            return newState;
        case 'DELETE_BOOKS_LIST':
            return [];
        default:
            return state;
    }
}

export default searchReducer;