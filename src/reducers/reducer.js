import searchReducer from './searchReducer';
import uniqueKeysPublisherReducer from './uniqueKeysPublisherReducer';
import filterPublisherReducer from './filterPublisherReducer';
import uniqueKeysYearReducer from './uniqueYearPublishedReducer';
import filterYearReducer from './filterYearReducer';
import sortStatusReducer from './SortStatusReducer';
import favouriteBooksReducer from './favouriteBooksReducer';
import searchErrorReducer from './searchErrorReducer';
import { combineReducers } from 'redux';

const reducer = combineReducers({
    booksCollection: searchReducer,
    uniqueCollectionPublisher: uniqueKeysPublisherReducer,
    filterPublisher: filterPublisherReducer,
    uniqueCollectionYear: uniqueKeysYearReducer,
    filterYear: filterYearReducer,
    sortStatus: sortStatusReducer,
    favouriteBooks: favouriteBooksReducer,
    searchError: searchErrorReducer
});

export default reducer;