const uniqueKeysPubliserReducer = (state = [], action) => {
    switch (action.type) {
        case 'UNIQUE_COLLECTION_PUBLISHER': {
            const getUnique = (arr) => {
                let result = [];
                for (let str of arr) {
                    if (!result.includes(str) && str) {
                        result.push(str);
                    }
                }

                return result;
            }
            let uniquePublisher = getUnique(action.data);
            return uniquePublisher;
        }
        case 'CLEAR_UNIQUE_PUBLISHER': {
            return [];
        }
        default:
            return state;
    }
}

export default uniqueKeysPubliserReducer;