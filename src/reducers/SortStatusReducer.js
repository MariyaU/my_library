const sortStatusReducer = (state = ['off', 'off', 'off'], action) => {
    switch (action.type) {
        case 'SORT_ACTIVE':
            const sortState = ['off', 'off', 'off'];
            if (isNaN(state[action.data])) {
                sortState[action.data] = 1;
            }
            else if (state[action.data] === 1) {
                sortState[action.data] = 0;
            }
            else if (state[action.data] === 0) {
                sortState[action.data] = 1;
            }
            return sortState;
            case 'CLEAR_SORT_STATUS':
                return ['off', 'off', 'off'];
            default:
                return state;
    }
}

export default sortStatusReducer;