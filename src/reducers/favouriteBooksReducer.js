const favouriteBooksReducer = (state = [], action) => {
    switch (action.type) {
        case 'ADD_FAVOURITE_BOOK':
            const copyState = state.slice();
            const index = action.data.map(item => item.id).indexOf(action.id);
            if (!(copyState.find(item => item.id === action.id))) {
                copyState.push(action.data[index])
            }
            return copyState;
        case 'DELETE_FAVOURITE_BOOK': {
            const index = state.findIndex(item => item.id === action.id);
            const copyBook = state.slice();
            copyBook.splice(index, 1);
            return copyBook;
        }
        default:
            return state;
    }
}

export default favouriteBooksReducer;