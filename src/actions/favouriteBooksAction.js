const favouriteBooks = (arr, id) => ({
        type: 'ADD_FAVOURITE_BOOK',
        data: [...arr],
        id: id
});

const deleteFavouriteBook = (id) => ({
    type: 'DELETE_FAVOURITE_BOOK',
    id: id
});

export {favouriteBooks, deleteFavouriteBook}