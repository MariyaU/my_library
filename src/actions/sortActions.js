const sortBooks = (index) => {
    return {
        type: 'SORT_ACTIVE',
        data : index
    }
}
export default sortBooks;

 export const clearSortStatus = () => {
    return {
        type: 'CLEAR_SORT_STATUS'
    }
};