export const filterPublisher = (type) => ({
        type: 'FILTER_PUBLISHER',
        data: type 
});

export const filterYear = (type) => ({
        type: 'FILTER_YEAR',
        data: type
});

export const removeFilterPublisher = () => ({
        type: 'REMOVE_FILTER_PUBLISHER'
});
export const removeFilterYear = () => ({
        type: 'REMOVE_FILTER_YEAR'
})