export const searchError = () => {
    return {
        type: 'SET_SEARCH_ERROR'
    }
};

export const searchErrorDelete = () => {
    return {
        type: 'DELETE_SEARCH_ERROR'
    }
};