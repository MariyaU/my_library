import React from 'react';

const Footer = () => {
    return (
        <footer className='footer-line bg-dark'>
            <span className='footer-content copyright'>Your library</span>
            <span className='footer-content'>2020</span>
        </footer>
    )
}
export default Footer