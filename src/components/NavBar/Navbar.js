import React from 'react';
import { connect } from 'react-redux';
import { Link, NavLink } from 'react-router-dom';
import { removeFilterPublisher, removeFilterYear } from '../../actions/filterActions';
import { deleteBooksList, clearUniguePublisherCollection, clearUnigueYearCollection } from '../../actions/getBooksAction';
import { searchErrorDelete } from '../../actions/SearchErrorActions';
import { clearSortStatus } from '../../actions/sortActions';

const Navbar = (props) => {
    const handleClick = () => {
        props.removeFilterYear();
        props.removeFilterPublisher();
        props.deleteBooksList();
        props.clearUniguePublisherCollection();
        props.clearUnigueYearCollection();
        props.searchErrorDelete();
        props.clearSortStatus();
    }
    return (
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
            <div className="container">
                <Link to='/' className="navbar-brand my-brand" onClick={handleClick}>Your library</Link>
                <ul className="nav justify-content-end">
                    <li className='nav-item active'> <NavLink to='/' className='nav-link'>Страница поиска</NavLink></li>
                    <li className='nav-item'><NavLink className='nav-link' to='/favourites'>Избранное</NavLink></li>
                </ul>
            </div>
        </nav>
    )
}
//  это жесть)
const mapDispatchToProps = (dispatch) => {
    return {
        removeFilterYear: () => dispatch(removeFilterYear()),
        removeFilterPublisher: () => dispatch(removeFilterPublisher()),
        deleteBooksList: () => dispatch(deleteBooksList()),
        clearUniguePublisherCollection: () => dispatch(clearUniguePublisherCollection()),
        clearUnigueYearCollection: () => dispatch(clearUnigueYearCollection()),
        searchErrorDelete: () => dispatch(searchErrorDelete()),
        clearSortStatus: () => dispatch(clearSortStatus())
    }
}

export default connect(null, mapDispatchToProps)(Navbar);