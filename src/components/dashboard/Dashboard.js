import React from 'react';
import MainScreen from './MainScreen';
import SideScreen from './SideScreen';

const Dashboard = () => {
        return (
            <div className='container mb-4 my-root'>
                <div className='row'>
                    <div className='col-8'><MainScreen /></div>
                    <div className='col-4'><SideScreen /></div>
                </div>
            </div>
        )
}

export default Dashboard;