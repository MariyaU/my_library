import React, { Component } from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import { searchResult, uniquePublisherCollection, uniqueYearCollection,  clearUniguePublisherCollection, clearUnigueYearCollection} from '../../actions/getBooksAction';
import { removeFilterPublisher, removeFilterYear } from '../../actions/filterActions';
import { searchError, searchErrorDelete } from '../../actions/SearchErrorActions'; 


class SearchForm extends Component {
    state = {
        search: '',
        apiKey: 'AIzaSyC5NTu7VPS-5F1Cn7XEXBPwTeKD2Yvj2ZY'
    }
    handleChange = (e) => {
        this.setState({
            search: e.target.value
        })
    }
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.removeFilterPublisher();
        this.props.removeFilterYear();
        axios.get("https://www.googleapis.com/books/v1/volumes?q=" + this.state.search + "&key=" + this.state.apiKey + "&maxREsults=10")
            .then(data => {
                this.props.setCollectionBooks(data.data.items);
                this.props.uniquePublisherCollection(data.data.items);
                this.props.uniqueYearCollection(data.data.items);
                this.props.searchErrorDelete();
            })
            .catch((err) => {
                this.props.clearPublisher();
                this.props.clearYear();
                this.props.searchError();
                console.log(err)
            })
    }
    render() {
        return (
            <div className='mt-4 container'>
                <form>
                    <div onSubmit={this.handleSubmit} className='form-group mb-0'>
                        <div className='row'>
                            <div className='input-group mb-2'>
                                <input type="text" className='form-control' id='booktitle' onChange={this.handleChange} />
                                <div className='input-group-append'>
                                    <button onClick={this.handleSubmit} className='btn btn-primary' type="submit" id="button-addon2">Поиск</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        setCollectionBooks: (data) => dispatch(searchResult(data)),
        uniquePublisherCollection: (data) => dispatch(uniquePublisherCollection(data)),
        uniqueYearCollection: (data) => dispatch(uniqueYearCollection(data)),
        removeFilterPublisher: () => dispatch(removeFilterPublisher()),
        removeFilterYear: () => dispatch(removeFilterYear()),
        clearPublisher: () => dispatch(clearUniguePublisherCollection()),
        clearYear: () => dispatch(clearUnigueYearCollection()),
        searchError: () => dispatch(searchError()),
        searchErrorDelete: () => dispatch(searchErrorDelete())
    }
}
export default connect(null, mapDispatchToProps)(SearchForm)