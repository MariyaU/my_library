import React, { Component } from 'react';
import { connect } from 'react-redux';
import sortBooks from '../../actions/sortActions';
import sort from '../../img/sort.png';


class SortElements extends Component {

    handleClickSort = (e) => {
        e.preventDefault();
        this.props.sortBooks(e.target.id);

    }
    render() {
        return (
            <div className='btn-group btn-group-sm' role='group'>
                <div className='align-help' key='0'>
                    <a className='btn sort-link' onClick={this.handleClickSort} key='0' id='0'><span className='link-title' id='0'>По названию</span>
                {this.props.sortStatus[0] === 1 ? <div className='my-arrow sort-image up' id='0' /> : this.props.sortStatus[0] === 0 ? <div className='sort-image down' id='0'/> : <img src={sort} alt='стрелка вверх' />}
                    </a>
                </div>
                <div className='align-help' key='1'>
                    <a className='btn sort-link' onClick={this.handleClickSort} key='1' id='1'><span className='link-title' id='1'>По дате</span>
                {this.props.sortStatus[1] === 1 ? <div className='my-arrow sort-image up' id='1'/> : this.props.sortStatus[1] === 0 ? <div className='sort-image down' id='1'/> : <img src={sort} alt='стрелка вверх' />}
                    </a>
                </div>
                <div className='align-help' key='2'>
                    <a className='btn sort-link' onClick={this.handleClickSort} key='2' id='2'><span className='link-title' id='2'>По рейтингу</span>
                {this.props.sortStatus[2] === 1 ? <div className='my-arrow sort-image up' id='2'/> : this.props.sortStatus[2] === 0 ? <div className='sort-image down' id='2'/> : <img src={sort} alt='стрелка вверх' />}
                    </a>
                </div>
            </div >
        )
    }
}

const mapStateToProps = (state) => {
    return {
        sortStatus: state.sortStatus
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        sortBooks: (index) => dispatch(sortBooks(index))
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(SortElements);