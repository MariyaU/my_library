import React from 'react';
import FilterElements from './FilterElements';

const SideScreen = () => {
    return (
        <div>
            <FilterElements />
        </div>
    )
}

export default SideScreen;