import React from 'react';
import SearchForm from './SearchForm';
import BooksList from './BooksList';
import SortElements from './SortElements';

const MainScreen = () => {
    return (
        <div>
            <SearchForm />
            <SortElements />
            <BooksList />
        </div>
    )
}

export default MainScreen;