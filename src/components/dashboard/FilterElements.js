import React, { Component } from 'react';
import { connect } from 'react-redux';
import { filterPublisher, filterYear, removeFilterPublisher, removeFilterYear } from '../../actions/filterActions';

class FilterElements extends Component {

    handleClickPublisher = (e) => {
        e.preventDefault();
        this.props.filterPublisher(e.target.value);

    }
    handleClickYear = (e) => {
        e.preventDefault();
        this.props.filterYear(e.target.value);
    }
    handleRemovePublisher = (e) => {
        e.preventDefault();
        this.props.removeFilterPublisher();
    }
    handleRemoveYear = (e) => {
        e.preventDefault();
        this.props.removeFilterYear();
    }

    render() {
        if (this.props.uniqueCollectionPublisher.length) {
            return (
                <>
                    <div className='mt-4 container col'>
                        <span>Выбрать издательство</span>
                        <div className='btn-group btn-group-toggle mt-3 d-flex flex-column' data-toggle="buttons">
                            {this.props.uniqueCollectionPublisher.map((item, index) => {
                                return (
                                    <label className='btn btn-secondary mb-2 d-flex justify-content-start' key={index} value={item}>
                                        <input type="radio" name="options" onClick={this.handleClickPublisher} value={item} id={index} /> {item} </label>
                                )
                            })}
                            <label className='btn btn-secondary mb-2 col-8' key='delete'>
                                <input type="radio" name="options" onClick={this.handleRemovePublisher} />Без фильтра</label>
                        </div>

                    </div>
                    <div className='mt-4 container col'>
                        <span>Выбрать год публикации</span>
                        <div className='btn-group btn-group-toggle mt-3 d-flex align-content-start flex-wrap' data-toggle="buttons">
                            {this.props.uniqueCollectionYear.map((item, index) => {
                                if (!(isNaN(item))) {
                                    return (
                                        <label className="btn btn-secondary mb-2 mr-1 col-4" value={item} key={index}>
                                            <input type="radio" name="options" onClick={this.handleClickYear} value={item} id={index} /> {item} </label>
                                    )
                                }
                                else
                                    return null;
                            })}
                                <label className="col-8 btn btn-secondary mb-2 mr-1 align-items-start btn-filter" key='deleteyear'>
                                    <input type="radio" name="options" onClick={this.handleRemoveYear} />Без фильтра</label>
                        </div>
                    </div>
                </>
            )
        }
        else return null
    }
}

const mapStatetoProps = (state) => {
    return {
        uniqueCollectionPublisher: state.uniqueCollectionPublisher,
        uniqueCollectionYear: state.uniqueCollectionYear,
        filterPublisherBtn: state.filterPublisher,
        filterYear: state.filterYear
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        filterPublisher: (data) => dispatch(filterPublisher(data)),
        filterYear: (data) => dispatch(filterYear(data)),
        removeFilterPublisher: () => dispatch(removeFilterPublisher()),
        removeFilterYear: () => dispatch(removeFilterYear())
    }
}

export default connect(mapStatetoProps, mapDispatchToProps)(FilterElements);